//
//  KAutoLevels.h
//  Kia
//
//  Created by Andrey on 06/05/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "KFilter.h"

@interface KAutoLevels : KFilter 
{
	CGFloat blackLevel;
	CGFloat whiteLevel;
}

@end
