//
//  KCumulativeHistogram.m
//  Kia
//
//  Created by Andrey on 25/03/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import "KCumulativeHistogram.h"
#import "KImage.h"
#import "KPixel.h"

@implementation KCumulativeHistogram

- (void) computeForImage: (KImage*)image
{
	[super computeForImage:image];
	
	for (int j = 0; j < IMAGE_CHANNEL_COUNT; j++)
	{
		for (int i = 1; i < intervalCount; i++)
		{
			channelHistograms[j].data[i] += channelHistograms[j].data[i - 1];
		}
		
		channelHistograms[j].maxPixelsPerInterval = image.width * image.height;
	}
/*	
	[self saveToTikzFile:@"/Users/Andrey/MSTU/Master/Paper/Design/UniformCumulativeHistogram.tex" 
			  forChannel: BrightnessImageChannel];
 */
}

@end
