//
//  KSimpleUIntList.h
//  Kia
//
//  Created by Andrey on 04/05/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface KSimpleUIntList : NSObject 
{
	NSUInteger* data;
	NSUInteger count;
	NSUInteger preallocatedItemCount;
}

@property (readonly) NSUInteger* data;
@property (readonly) NSUInteger count;

- (id) init;
+ (KSimpleUIntList*) list;
- (id) initWithCapacity: (NSUInteger)capacity;
+ (KSimpleUIntList*) listWithCapacity: (NSUInteger)capacity;
- (void) add: (NSUInteger)value;
- (void) removeAllElements;
- (void) dealloc;

@end
