//
//  KImageProcessor.m
//  Kia
//
//  Created by Andrey on 28/05/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import "KImageProcessor.h"
#import "KImage.h"
#import "KImageLayer.h"
#import "KPixel.h"
#import "KFilter.h"
#import "KAutoLevels.h"
#import "KCurve.h"
#import "KCurves.h"
#import "KEqualizeChannel.h"
#import "KFadeFilter.h"
#import "KSegmentFilter.h"

@implementation KImageProcessor

#define DEFAULT_AUTOLEVELS_THRESHOLD 0.001
#define CorrectionChannel BrightnessImageChannel

+ (KFilter*) getDefaultFilterOfType: (enum KFilterType)type
{
	NSNumber* affectedChannel = 
	[NSNumber numberWithUnsignedInt:CorrectionChannel];
	
	switch (type) 
	{
		case AutoLevels:
		{
			NSNumber* rejectionThresholdForShadows = 
			[NSNumber numberWithFloat:DEFAULT_AUTOLEVELS_THRESHOLD];
			NSNumber* rejectionThresholdForHighlights = 
			[NSNumber numberWithFloat:DEFAULT_AUTOLEVELS_THRESHOLD];
			
			return [KAutoLevels filterWithParameters:
					[NSDictionary dictionaryWithObjectsAndKeys:
					 rejectionThresholdForShadows, 
					 @"RejectionThresholdForShadows", 
					 rejectionThresholdForHighlights, 
					 @"RejectionThresholdForHighlights", 
					  affectedChannel,
					 @"AffectedChannel", nil
					 ]];
		}
		case Curves:
		{
			KCurve* curve = [[[KCurve alloc] init] autorelease];
			[curve addControlPoint:NSMakePoint(0, 0)];
			[curve addControlPoint:NSMakePoint(1.0, 1.0)];
			[curve addControlPoint:NSMakePoint(0.2, 0.1)];
			[curve addControlPoint:NSMakePoint(0.8, 0.9)];
			
			return [KCurves filterWithParameters:
					[NSDictionary dictionaryWithObjectsAndKeys:
					  curve,
					 @"Curve",
					 affectedChannel,
					 @"AffectedChannel", nil
					 ]];
		}
		case Equalize:
		{
			return [KEqualizeChannel filterWithParameters:
					[NSDictionary dictionaryWithObjectsAndKeys:
					 affectedChannel, 
					 @"ChannelToEqualize", nil
					 ]];
		}
			break;
			
		default:
			return nil;
	}
}

+ (Boolean) processImage:(KImage*)image usingTemporaryLayerForFilter:(KFilter*)filter withMultiplier: (CGFloat)multiplier
{
	// Construct intermediate filter
	KFilter* intermediateFilter;
	if (multiplier < 1.0 && multiplier > 0)
	{
		intermediateFilter = [KFadeFilter filterWithParameters:
							  [NSDictionary dictionaryWithObjectsAndKeys:
							   filter, 
							   @"Filter", 
							   [NSNumber numberWithFloat:multiplier], 
							   @"Fraction", nil
							   ]];
	}
	else if (multiplier == 1.0)
		intermediateFilter = filter;
	else if (multiplier == 0.0)
		return NO;
	
	// Add temporary layer
	KImageLayer* newLayer =	[[KImageLayer alloc] initWithLayer: [image layerAtIndex:image.indexOfSelectedLayer]];
	[image addLayerOnTop:newLayer];
	[newLayer release];
	
	// Process image with intermediate filter
	[intermediateFilter processImage:image inPlace:YES];
	[image recomputeStatistics];
	
	NSLog(@"Applied %@", [filter name]);
	NSLog(@"%@", image);
	return YES;
}

+ (void) processImage:(KImage*)image usingFilter:(KFilter*)filter
{
	[filter processImage:image inPlace:YES];
	[image syncWithPixels];
}

+ (void) processImage:(KImage*)image usingFilterChain:(NSArray*)filterChain
{
	for (KFilter* filter in filterChain)
	{
		[filter processImage:image inPlace:YES];
		[image recomputeStatistics];
	}
	
	[image syncWithPixels];
}

@end
