//
//  KImageLayer.h
//  Kia
//
//  Created by Andrey on 29/04/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "KPixel.h"

enum KImageLayerType
{
	NormalLayer,
	AdjustmentLayer,
	MaskLayer
};

@class KSimpleFloatArray;

@interface KImageLayer : NSObject 
{
	// Dimensions
	NSUInteger width;
	NSUInteger height;
	
	// Pixel data
	KPixel** pixels;
	
	// Layer properties
	NSString* name;
	CGFloat opacity;
	Boolean visible;
	enum KPixelBlendMode blendMode;
	enum KImageLayerType type;
}

@property (readonly) NSUInteger width;
@property (readonly) NSUInteger height;
@property (retain) NSString* name;
@property CGFloat opacity;
@property Boolean visible;
@property enum KPixelBlendMode blendMode;
@property enum KImageLayerType type;

- (id) init;
- (id) initWithWidth: (NSUInteger)aWidth andHeight: (NSUInteger)aHeight;
- (id) initWithLayer: (KImageLayer*)layer;
- (id) initWithLayer: (KImageLayer*)layer andMask: (KSimpleFloatArray*)mask;
- (void) fillWithPixels: (KPixel*) samplePixel;
- (KPixel*) pixelAtX: (NSUInteger)x y: (NSUInteger)y;
- (void) setPixel: (KPixel*)pixel atX: (NSUInteger)x y: (NSUInteger)y;
- (void) dealloc;

@end
