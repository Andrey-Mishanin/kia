//
//  ChromaticityHistogramView.h
//  Kia
//
//  Created by Andrey on 26/04/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "KiaController.h"

@interface ChromaticityHistogramView : NSView 
{
	IBOutlet KiaController* controller;
	NSPoint highlightedCell;
	CGFloat colorContrast;
}

@property NSPoint highlightedCell;
@property (readonly) CGFloat colorContrast;

@end
