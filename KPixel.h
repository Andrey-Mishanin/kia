//
//  KPixel.h
//  Kia
//
//  Created by Andrey on 26/04/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#define IMAGE_CHANNEL_COUNT 9

enum KImageChannel 
{
	RedImageChannel,
	GreenImageChannel,
	BlueImageChannel,
	HueImageChannel,
	SaturationImageChannel,
	BrightnessImageChannel,
	LightnessImageChannel,
	AImageChannel,
	BImageChannel
};

enum KChromaticityChannel
{
	HueChromaticityChannel,
	ABChromaticityChannel
};

enum KPixelRepType
{
	RGBRep,
	HSBRep,
	CIELabRep
};

enum KPixelBlendMode 
{
	NormalBlendMode,
	BrightnessBlendMode
};

typedef struct _RGBPixelRep 
{
	CGFloat red;
	CGFloat green;
	CGFloat blue;
} RGBPixelRep;

typedef struct _HSBPixelRep
{
	CGFloat hue;
	CGFloat saturation;
	CGFloat brightness;
} HSBPixelRep;

typedef struct _CIELabPixelRep 
{
	CGFloat lightness;
	CGFloat a;
	CGFloat b;
} CIELabPixelRep;

CIELabPixelRep MakeCIELabPixelRep(CGFloat lightness, CGFloat a, CGFloat b);

typedef struct _KRedundantColorVector 
{
	CGFloat data[IMAGE_CHANNEL_COUNT];
} KRedundantColorVector;	
	
@interface KPixel : NSObject <NSCopying>
{
	RGBPixelRep _rgbRep;
	HSBPixelRep _hsbRep;
	CIELabPixelRep _cieLabRep;
	
	NSUInteger referenceCount;
	NSString* owner;
}

@property (copy) NSString* owner;

- (id) init;
- (id) initWithRed: (CGFloat)red green: (CGFloat)green andBlue: (CGFloat)blue;
- (id) initWithHue: (CGFloat)hue saturation: (CGFloat) saturation andBrightness: (CGFloat)brightness;
- (id) initWithLightness: (CGFloat)lightness a: (CGFloat)a andB: (CGFloat)b;
- (id) initWithNSColor: (NSColor*)color;
- (void) dealloc;

/* Accessors */
- (NSColor*) nsColor;
- (CGFloat) red;
- (CGFloat) green;
- (CGFloat) blue;
- (CGFloat) hue;
- (CGFloat) saturation;
- (CGFloat) brightness;
- (CGFloat) lightness;
- (CGFloat) a;
- (CGFloat) b;
- (CIELabPixelRep) cieLabRep;
- (KRedundantColorVector) redundantColorVector;

/* Methods */
- (KPixel*) blendedPixelWithFraction:(CGFloat)fraction 
							 ofPixel:(KPixel*)anotherPixel
					  usingBlendMode:(enum KPixelBlendMode)blendMode;
- (void) linearBlendWithFraction: (CGFloat)fraction 
						 ofPixel: (KPixel*)anotherPixel 
				  usingBlendMode:(enum KPixelBlendMode)blendMode;
- (void) setValue: (CGFloat)value forChannel: (enum KImageChannel)channel;

@end
