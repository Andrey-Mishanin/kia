//
//  K2DHistrogram.h
//  Kia
//
//  Created by Andrey on 27/04/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "KHistogram.h"

@interface K2DHistrogram : KHistogram 
{

}

- (void) computeForImage: (KImage*)image;

@end
