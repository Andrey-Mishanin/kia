//
//  KFilterOperation.m
//  Kia
//
//  Created by Andrey on 06/05/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import "KFilterOperation.h"
#import "KFilter.h"

@implementation KFilterOperation

- (id) initWithImage: (KImage*)image andFilter: (KFilter*)aFilter
{
	if (self = [super init])
	{
		imageToProcess = image;
		filter = aFilter;
	}
	
	return self;
}

- (void) main
{
	[filter processImage:imageToProcess inPlace:YES];
	NSLog(@"Applied %@", [filter name]);
}

@end
