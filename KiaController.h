//
//  KiaController.h
//  Kia
//
//  Created by Andrey on 03/02/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "KImage.h"
#import "KPixel.h"

@class KiaView;
@class CurvesView;
@class HistogramView;
@class KSegmentationMap;
@class K2DSegmentationMap;
@class ChromaticityHistogramView;
@class KFilter;

@interface KiaController : NSObject
{
	/* Interface Builder outlets */
	/* Main window */
	IBOutlet KiaView* mainView;
	IBOutlet NSScrollView* scrollView;
	IBOutlet NSWindow* window;
	
	/* Histrogram window */
	IBOutlet NSWindow* histogramWindow;
	IBOutlet HistogramView* histogramView;
	IBOutlet NSTextField* meanLabel;
	IBOutlet NSTextField* varianceLabel;
	IBOutlet NSTextField* uniformityLabel;
	IBOutlet NSComboBox* histogramChannelComboBox;
	IBOutlet NSButton* cumulativeHistogramCheckbox;
	
	/* Filters window */
	IBOutlet NSWindow* filtersWindow;
	IBOutlet NSTabView* filterParametersTabView;
	IBOutlet NSTextField* rejectionThresholdForShadowsTextField;
	IBOutlet NSTextField* rejectionThresholdForHighlightsTextField;
	IBOutlet CurvesView* curvesView;
	IBOutlet NSSlider* redChannelFractionSlider;
	IBOutlet NSSlider* greenChannelFractionSlider;
	IBOutlet NSSlider* blueChannelFractionSlider;
	IBOutlet NSComboBox* activeSegmentComboBox;
	IBOutlet NSComboBox* affectedChannelComboBox;
	IBOutlet NSSlider* fadeFilterSlider;
	
	/* Segmentation window */
	IBOutlet NSWindow* segmentationWindow;
	IBOutlet NSComboBox* channelForSegmentationComboBox;
	IBOutlet NSTextField* segmentCountTextField;
	
	/* Chromaticity histogram window */
	IBOutlet NSWindow* chromaticityHistogramWindow;
	IBOutlet ChromaticityHistogramView* chromaticityHistogramView;
	IBOutlet NSComboBox* chromaticityHistogramChannelComboBox;
	
	/* Layers window */
	IBOutlet NSWindow* layersWindow;
	IBOutlet NSTableView* layersTableView;
	
	/* Auto Enhancement window */
	IBOutlet NSWindow* autoEnhancementWindow;
	IBOutlet NSComboBox* importantSegmentComboBox;
	
	/* Data */
	KImage* currentImage;
	KImage* displayBuffer;
	
	/* Segmentation */
	KSegmentationMap* segmentationMap;
	K2DSegmentationMap* chromaticitySegmentationMap;
	Boolean useChromaticityForSegmentation;
	enum KImageChannel channelForSegmentation;

	/* Histogram */
	enum KImageChannel displayedHistogramChannel;
	Boolean displayCumulativeHistogram;
	enum KChromaticityChannel displayedChromaticityHistogramChannel;
}

@property (readonly) KImage* currentImage;
@property (readonly) KImage* displayBuffer;
@property (readonly) KSegmentationMap* segmentationMap;
@property (readonly) K2DSegmentationMap* chromaticitySegmentationMap;
@property (readonly) enum KImageChannel channelForSegmentation;
@property (readonly) enum KImageChannel displayedHistogramChannel;
@property (readonly) enum KChromaticityChannel displayedChromaticityHistogramChannel;
@property (readonly) Boolean useChromaticityForSegmentation;
@property (readonly) Boolean displayCumulativeHistogram;
@property (readonly) NSWindow* histogramWindow;

- (IBAction) openDocument: (id) sender;
- (IBAction) saveDocument: (id) sender;
- (IBAction) restoreCurrentImageFromBackup: (id) sender;

- (IBAction) showHistogramWindow: (id) sender;
- (IBAction) changeDisplayedHistogramChannel: (id) sender;
- (IBAction) changeHistogramType: (id) sender;

- (IBAction) showSegmentationWindow: (id) sender;
- (IBAction) changeSegmentationSource: (id) sender;
- (IBAction) segmentImage: (id) sender;

- (IBAction) showChromaticityHistogramWindow: (id) sender;
- (IBAction) changeDisplayedChromaticityHistogramChannel: (id) sender;

/* Auto enhancement window */
- (IBAction) showAutoEnhancementWindow: (id) sender;
- (IBAction) autoEnhanceCurrentImage: (id) sender;
- (IBAction) writeOutVariants: (id)sender;

/* Layers window */
- (IBAction) showLayersWindow: (id) sender;
- (IBAction) addNewLayer: (id) sender;
- (IBAction) cloneCurrentLayerAndPutItOnTop: (id) sender;
- (IBAction) removeSelectedLayer: (id) sender;

/* Filters window */
- (IBAction) showFiltersWindow: (id) sender;
- (IBAction) applyFilter: (id) sender;
- (IBAction) cancelAndCloseFiltersWindow: (id) sender;

- (id) init;
- (void) trackMouseDragging: (NSPoint)mouseLocation inView: (NSView*)view;
- (void) mouseClicked: (NSPoint)mouseLocation;
- (void) backupCurrentImage;
- (void) processCurrentImageUsingFilter: (KFilter*)filter;
- (void) processCurrentImageUsingFilterChain: (NSArray*)filterChain;
- (void) updateHistogramStatistics;
- (void) dealloc;

@end
