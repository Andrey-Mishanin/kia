//
//  KImageSegmentator.h
//  Kia
//
//  Created by Andrey on 27/03/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class KImage;
@class KSegmentationMap;
@class K2DSegmentationMap;

@interface KImageSegmentator : NSObject 
{

}

+ (KSegmentationMap*) segmentImage: (KImage*)image 
	   usingKMeansWithSegmentCount: (NSUInteger)segmentCount 
				   andUsingChannel: (enum KImageChannel)channel;

+ (K2DSegmentationMap*) segmentImageByChromaticity: (KImage*)image 
					   usingKMeansWithSegmentCount: (NSUInteger)segmentCount;

@end
