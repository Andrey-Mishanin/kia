//
//  KFilter.m
//  Kia
//
//  Created by Andrey on 06/05/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import "KFilter.h"

@implementation KFilter

- (id) initWithParameters: (NSDictionary*) filterParameters
{
	if (self = [super init])
	{
		parameters = [filterParameters retain];
		currentImage = nil;
	}
	
	return self;
}

+ (KFilter*) filterWithParameters: (NSDictionary*) filterParameters
{
	return [[[[self class] alloc] initWithParameters:filterParameters] autorelease];
}

- (void) reset
{
	currentImage = nil;
}

- (KPixel*) processPixel: (KPixel*)pixel ofImage: (KImage*)image atX: (NSUInteger)x y: (NSUInteger)y
{
	return pixel;
}

- (KImage*) processImage: (KImage*)image inPlace: (Boolean)inPlace
{
	return image;
}

- (NSString*) name
{
	return @"Generic Filter";
}

+ (NSString*) getNameByType: (enum KFilterType)type
{
	switch (type) 
	{
		case AutoLevels:
			return @"AutoLevels";
		case Curves:
			return @"Curves";
		case Equalize:
			return @"Equalize";
		case Desaturate:
			return @"Desaturate";
		default:
			return @"";
	}
}

- (void) dealloc
{
	[parameters release];
	[super dealloc];
}

@end
