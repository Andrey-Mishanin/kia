//
//  KiaView.m
//  Kia
//
//  Created by Andrey on 03/02/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "KiaView.h"

@implementation KiaView

- (id)initWithFrame:(NSRect)frame 
{
    self = [super initWithFrame:frame];

    if (self) 
	{
		
    }
	
    return self;
}

- (BOOL)acceptsFirstResponder
{
	return YES;
}

- (void)mouseDragged:(NSEvent *)theEvent
{
	NSPoint mouseLocation = 
	[self convertPoint:[theEvent locationInWindow] fromView:nil];
	
	[controller trackMouseDragging:mouseLocation inView: self];
}

- (void)mouseDown:(NSEvent*)theEvent
{
	if ([theEvent modifierFlags] & NSCommandKeyMask)
	{
		NSPoint mouseLocation = 
		[self convertPoint:[theEvent locationInWindow] fromView:nil];
		
		[controller mouseClicked:mouseLocation];
	}
}

- (void)drawRect:(NSRect)rect 
{
	if (controller.displayBuffer != nil)
	{
		NSRect rectToDrawIn = 
		NSMakeRect(0, 0, controller.displayBuffer.width, controller.displayBuffer.height);
		
		[[controller.displayBuffer bitmap] drawInRect:rectToDrawIn];
	}
}

- (void) dealloc
{
	[super dealloc];
}
@end