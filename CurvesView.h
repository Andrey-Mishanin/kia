//
//  CurvesView.h
//  Kia
//
//  Created by Andrey on 03/04/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class KCurve;
@class KiaController;

@interface CurvesView : NSView 
{
	IBOutlet KiaController* controller;
	KCurve* curve;
	NSUInteger selectedCurveControlPoint;
	CGFloat highlightedLevel;
}

@property (readonly) KCurve* curve;
@property (readonly) NSUInteger selectedCurveControlPoint;
@property CGFloat highlightedLevel;

- (id) initWithFrame: (NSRect)frame;
- (void) drawRect: (NSRect)rect;
- (void) dealloc;
@end
