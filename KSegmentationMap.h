//
//  KSegmentationMap.h
//  Kia
//
//  Created by Andrey on 18/04/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "KPixel.h"

@class KSimpleUIntArray;
@class KSimpleFloatArray;

@interface KSegmentationMap : NSObject 
{
	NSUInteger segmentCount;
	KSimpleUIntArray* crispSegmentationMap;
	KSimpleFloatArray* centroids;
	KSimpleFloatArray** segments;
}

@property (readonly) NSUInteger segmentCount;
@property (readonly) KSimpleUIntArray* crispSegmentationMap;
@property (readonly) KSimpleFloatArray* centroids;
@property (readonly) KSimpleFloatArray** segments;

- (id) init;
- (id) initWithPixelCount: (NSUInteger)pixelCount andSegmentCount: (NSUInteger)aSegmentCount;
- (void) dealloc;
@end

@interface K2DSegmentationMap : KSegmentationMap
{
	CIELabPixelRep* centroidPoints;
}

@property (readonly) CIELabPixelRep* centroidPoints;

- (id) init;
- (id) initWithPixelCount: (NSUInteger)pixelCount andSegmentCount: (NSUInteger)aSegmentCount;
- (void) dealloc;

@end
