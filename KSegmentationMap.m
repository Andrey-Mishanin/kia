//
//  KSegmentationMap.m
//  Kia
//
//  Created by Andrey on 18/04/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import "KSegmentationMap.h"
#import "KSimpleFloatArray.h"
#import "KSimpleUIntArray.h"

@implementation KSegmentationMap
@synthesize segmentCount;
@synthesize crispSegmentationMap;
@synthesize centroids;
@synthesize segments;

- (id) init
{
	if (self = [super init])
	{
		
	}
	
	return self;
}

- (id) initWithPixelCount: (NSUInteger)pixelCount andSegmentCount: (NSUInteger)aSegmentCount
{
	if (self = [super init])
	{
		segmentCount = aSegmentCount;
		
		crispSegmentationMap = [[KSimpleUIntArray arrayWithCapacity:pixelCount] retain]; 
		
		centroids = [[KSimpleFloatArray arrayWithCapacity:segmentCount] retain];
		
		segments = (KSimpleFloatArray**)malloc(sizeof(KSimpleFloatArray*) * segmentCount);
		for (int i = 0; i < segmentCount; i++)
		{
			segments[i] = [[KSimpleFloatArray arrayWithCapacity:pixelCount] retain];
		}
	}
	
	return self;
}

- (void) dealloc
{
	for (int i = 0; i < segmentCount; i++)
	{
		[segments[i] release];
	}
	
	free(segments);
	[centroids release];
	[crispSegmentationMap release];
	
	[super dealloc];
}
@end

@implementation K2DSegmentationMap
@synthesize centroidPoints;

- (id) init
{
	if (self = [super init])
	{
		
	}
	
	return self;
}

- (id) initWithPixelCount: (NSUInteger)pixelCount andSegmentCount: (NSUInteger)aSegmentCount
{
	if (self = [super initWithPixelCount:pixelCount andSegmentCount:aSegmentCount])
	{
		centroidPoints = (CIELabPixelRep*)malloc(sizeof(CIELabPixelRep) * segmentCount);
		bzero(centroidPoints, sizeof(NSPoint) * segmentCount);
	}
	
	return self;
}

- (void) dealloc
{
	free(centroidPoints);
	[super dealloc];
}
@end