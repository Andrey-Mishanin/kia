//
//  KCurves.m
//  Kia
//
//  Created by Andrey on 06/05/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import "KCurves.h"
#import "KImage.h"
#import "KPixel.h"
#import "KCurve.h"

@implementation KCurves

- (KPixel*) processPixel: (KPixel*)pixel ofImage: (KImage*)image atX: (NSUInteger)x y: (NSUInteger)y
{
	enum KImageChannel affectedChannel = GetParameterUIntValue(@"AffectedChannel");
	KCurve* curve = [parameters objectForKey:@"Curve"];
	
	CGFloat channelValue = [pixel redundantColorVector].data[affectedChannel];
	channelValue = [curve valueAt:channelValue];
	
	[pixel setValue:channelValue forChannel:affectedChannel];
	
	return pixel;
}

- (KImage*) processImage: (KImage*)image inPlace: (Boolean)inPlace
{
	enum KImageChannel affectedChannel = GetParameterUIntValue(@"AffectedChannel");
	KCurve* curve = [parameters objectForKey:@"Curve"];
	
	ForEachImagePixel(image)
	{
		KPixel* currentPixel = [image pixelAtX:x y:y];
		
		CGFloat channelValue = [currentPixel redundantColorVector].data[affectedChannel];
		channelValue = [curve valueAt:channelValue];

		[currentPixel setValue:channelValue forChannel:affectedChannel];
	}
	
	[self reset];
	
	return image;
}

- (NSString*) name
{
	return @"Curves";
}

- (void) dealloc
{
	[super dealloc];
}

@end
