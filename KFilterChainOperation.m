//
//  KFilterChainOperation.m
//  Kia
//
//  Created by Andrey on 06/05/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import "KFilterChainOperation.h"
#import "KFilter.h"

@implementation KFilterChainOperation

- (id) initWithImage: (KImage*)image andFilterChain: (NSArray*)aFilterChain
{
	if (self = [super init])
	{
		imageToProcess = image;
		filterChain = [aFilterChain retain];
	}
	
	return self;
}

- (void)main
{
	NSLog(@"At KFilterChainOperation::main");
	for (KFilter* filter in filterChain)
	{
		[filter processImage:imageToProcess inPlace:YES];
		NSLog(@"Applied %@", [filter name]);
	}
}

@end
