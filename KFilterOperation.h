//
//  KFilterOperation.h
//  Kia
//
//  Created by Andrey on 06/05/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class KImage;
@class KFilter;

@interface KFilterOperation : NSOperation
{
	KImage* imageToProcess;
	KFilter* filter;
}

- (id) initWithImage: (KImage*)image andFilter: (KFilter*)aFilter;

@end
