//
//  KSegmentFilter.m
//  Kia
//
//  Created by Andrey on 08/05/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import "KSegmentFilter.h"
#import "KPixel.h"
#import "KImage.h"
#import "KSimpleFloatArray.h"
#import "KFadeFilter.h"

@implementation KSegmentFilter

- (KPixel*) processPixel: (KPixel*)pixel ofImage: (KImage*)image atX: (NSUInteger)x y: (NSUInteger)y		
{	
	KFilter* filter = [parameters objectForKey:@"Filter"];
	KSimpleFloatArray* segment = [parameters objectForKey:@"Segment"];
	NSUInteger index = y * image.width + x;
	
	KFadeFilter* fadeFilter = 
	[KFadeFilter filterWithParameters:
	 [NSDictionary dictionaryWithObjectsAndKeys:
	  [NSNumber numberWithFloat:segment.data[index]],
	  @"Fraction",
	  filter,
	  @"Filter", nil
	  ]];
	
	[fadeFilter processPixel:pixel ofImage:image atX:x y:y];
	
	return pixel;
}

- (KImage*) processImage: (KImage*)image inPlace: (Boolean)inPlace
{
	
	ForEachImagePixel(image)
	{
		NSAutoreleasePool* subPool = [[NSAutoreleasePool alloc] init];
		[self processPixel:[image pixelAtX:x y:y] ofImage:image atX:x y:y];
		[subPool release];
	}
	
	return image;
}

- (NSString*) name
{
	return @"Segment Filter";
}

- (void) dealloc
{
	[super dealloc];
}

@end
