//
//  KImage.h
//  Kia
//
//  Created by Andrey on 22/03/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
/*
#import "KHistogram.h"
#import "KCumulativeHistogram.h"
#import "K2DHistrogram.h"
*/

#define ForEachImagePixel(image) for (NSUInteger x = 0; x < image.width; x++) \
for (NSUInteger y = 0; y < image.height; y++)

@class KPixel;
@class KHistogram;
@class KCumulativeHistogram;
@class K2DHistrogram;
@class KImageLayer;

@interface KImage : NSObject 
{
	// Dimensions
	NSUInteger width;
	NSUInteger height;
	
	// Pixel data
	NSMutableArray* layers;
	NSUInteger indexOfSelectedLayer;
	NSBitmapImageRep* cachedRenderBitmap;
	
	// Histogram
	Boolean needRecomputeHistogram;
	Boolean needRenderBitmap;
	KHistogram* histogram;
	KCumulativeHistogram* cumulativeHistogram;
	K2DHistrogram* cieABHistogram;
}

@property (readonly) NSUInteger width;
@property (readonly) NSUInteger height;
@property NSUInteger indexOfSelectedLayer;
@property Boolean needRenderBitmap;
@property (readonly) KHistogram* histogram;
@property (readonly) KCumulativeHistogram* cumulativeHistogram;
@property (readonly) K2DHistrogram* cieABHistogram;

- (id) init;
- (id) initWithWidth: (NSUInteger)aWidth andHeight: (NSUInteger)aHeight;
- (id) initWithFile: (NSString*)filename;

- (void) addLayerOnTop: (KImageLayer*)newLayer;
- (void) insertLayer: (KImageLayer*)newLayer atIndex: (NSUInteger)index;
- (KImageLayer*) layerAtIndex: (NSUInteger)index;
- (NSUInteger) layerCount;
- (void) removeLayerFromTop;
- (void) removeLayerAtIndex: (NSUInteger)index; 

- (KPixel*) pixelAtX: (NSUInteger)x y: (NSUInteger)y;
- (KPixel*) pixelForRenderAtX: (NSUInteger)x y: (NSUInteger)y;
- (void) setPixel: (KPixel*)pixel atX: (NSUInteger)x y: (NSUInteger)y;

- (NSBitmapImageRep*) bitmap;
- (void) renderToBitmap;

- (CGFloat) contrast;
- (CGFloat) uniformity;
- (CGFloat) colorContrast;

- (void) saveToTIFF: (NSString*)filename;
- (void) restoreFromBackup: (NSString*)backupFilename;
- (void) recomputeStatistics;
- (void) syncWithPixels;
- (void) dealloc;
@end
