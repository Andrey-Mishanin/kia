//
//  main.m
//  Kia
//
//  Created by Andrey on 05/05/2009.
//  Copyright Karma Software 2009. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Foundation/NSDebug.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc,  (const char **) argv);
}
