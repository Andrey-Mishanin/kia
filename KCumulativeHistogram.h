//
//  KCumulativeHistogram.h
//  Kia
//
//  Created by Andrey on 25/03/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "KHistogram.h"

@interface KCumulativeHistogram : KHistogram 
{

}

- (void) computeForImage: (KImage*)image;

@end
