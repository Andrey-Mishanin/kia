//
//  KSimpleFloatArray.m
//  Kia
//
//  Created by Andrey on 18/04/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import "KSimpleFloatArray.h"


@implementation KSimpleFloatArray

@synthesize data;
@synthesize count;

- (id) init
{
	if (self = [super init])
	{
		data = NULL;
		count = 0;
	}
	
	return self;
}

- (id) initWithCapacity: (NSUInteger)capacity
{
	if (self = [super init])
	{
		count = capacity;
		data = (CGFloat*)malloc(sizeof(CGFloat) * capacity);
		bzero(data, sizeof(CGFloat) * capacity);
	}
	
	return self;
}

+ (KSimpleFloatArray*) arrayWithCapacity: (NSUInteger)capacity
{
	KSimpleFloatArray* newArray = [[KSimpleFloatArray alloc] initWithCapacity:capacity];
	
	return [newArray autorelease];
}

- (CGFloat) maxValue
{
	CGFloat maxValue = -1e30;
	
	for (int i = 0; i < count; i++)
	{
		if (data[i] > maxValue)
			maxValue = data[i];
	}
	
	return maxValue;
}

- (NSUInteger) maxValueIndex
{
	CGFloat maxValue = -1e30;
	NSUInteger maxValueIndex = -1;
	
	for (int i = 0; i < count; i++)
	{
		if (data[i] > maxValue)
		{
			maxValue = data[i];
			maxValueIndex = i;
		}
	}
	
	return maxValueIndex;
}

- (CGFloat) minValue
{
	CGFloat minValue = 1e30;
	
	for (int i = 0; i < count; i++)
	{
		if (data[i] < minValue)
			minValue = data[i];
	}
	
	return minValue;
}

- (NSUInteger) minValueIndex
{
	CGFloat minValue = 1e30;
	NSUInteger minValueIndex = -1;
	
	for (int i = 0; i < count; i++)
	{
		if (data[i] < minValue)
		{
			minValue = data[i];
			minValueIndex = i;
		}
	}
	
	return minValueIndex;
}

- (void) dealloc
{
	free(data);
	data = NULL;
	count = 0;
	
	[super dealloc];
}

@end
