//
//  KFadeFilter.m
//  Kia
//
//  Created by Andrey on 08/05/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import "KFadeFilter.h"
#import "KPixel.h"
#import "KImage.h"

@implementation KFadeFilter

- (KPixel*) processPixel: (KPixel*)pixel ofImage: (KImage*)image atX: (NSUInteger)x y: (NSUInteger)y
{
	CGFloat fraction = GetParameterFloatValue(@"Fraction");
	KFilter* filter = [parameters objectForKey:@"Filter"];
	
	KPixel* pixelCopy = [pixel copy];
	pixelCopy.owner = @"KFadeFilter::processPixel:ofImage:atX:y";
	[filter processPixel:pixelCopy ofImage:image atX: x y:y];
	
	[pixel linearBlendWithFraction:fraction ofPixel:pixelCopy usingBlendMode:NormalBlendMode];
	
	[pixelCopy release];
	
	return pixel;
}

- (KImage*) processImage: (KImage*)image inPlace: (Boolean)inPlace
{
	ForEachImagePixel(image)
	{
		KPixel* currentPixel = [image pixelAtX:x y:y];
		[self processPixel:currentPixel ofImage:image atX:x y:y];
	}
	
	return image;
}

- (NSString*) name
{
	return @"Fade Filter";
}

- (void) dealloc
{
	[super dealloc];
}

@end
