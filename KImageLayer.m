//
//  KImageLayer.m
//  Kia
//
//  Created by Andrey on 29/04/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import "KImageLayer.h"
#import "KImage.h"
#import "KSimpleFloatArray.h"

@implementation KImageLayer

@synthesize width;
@synthesize height;
@synthesize name;
@synthesize opacity;
@synthesize visible;
@synthesize blendMode;
@synthesize type;

- (id) init
{
	if (self = [super init])
	{
		width = height = 0;
		pixels = nil;
		name = @"New layer";
		opacity = 1.0;
		visible = NO;
		blendMode = NormalBlendMode;
		type = NormalLayer;
	}
	
	return self;
}

- (id) initWithWidth: (NSUInteger)aWidth andHeight: (NSUInteger)aHeight
{
	if (self = [super init])
	{
		width = aWidth;
		height = aHeight;

		pixels = (KPixel**)calloc(width * height, sizeof(KPixel*));
		
		name = @"New layer";
		opacity = 1.0;
		visible = YES;
		blendMode = NormalBlendMode;
		type = NormalLayer;
	}
	
	return self;
}

- (id) initWithLayer: (KImageLayer*)layer
{
	if (self = [super init])
	{
		width = layer.width;
		height = layer.height;
		
		pixels = (KPixel**)calloc(width * height, sizeof(KPixel*));
		
		ForEachImagePixel(self)
		{
			KPixel* sourcePixel = [[layer pixelAtX:x y:y] copy];
			sourcePixel.owner = @"KImageLayer::initWithLayer";
			[self setPixel:sourcePixel atX:x y:y];
			[sourcePixel release];
		}
		
		name = [[NSString stringWithFormat:@"%@ (Copy)", layer.name] retain];
		opacity = 1.0;
		visible = YES;
		blendMode = NormalBlendMode;
		type = NormalLayer;
	}
	
	return self;
}

- (id) initWithLayer: (KImageLayer*)layer andMask: (KSimpleFloatArray*)mask
{
	if (self = [super init])
	{
		width = layer.width;
		height = layer.height;
		
		pixels = (KPixel**)calloc(width * height, sizeof(KPixel*));
		
		KPixel* backgroundPixel = [[KPixel alloc] initWithRed:0 green:0 andBlue:0];
		backgroundPixel.owner = @"KImageLayer::initWithLayer:andMask";
		ForEachImagePixel(self)
		{
			NSUInteger index = y * width + x;
			
			KPixel* newPixel = 
			[[layer pixelAtX:x y:y] blendedPixelWithFraction:mask.data[index] 
													 ofPixel:backgroundPixel 
											  usingBlendMode:NormalBlendMode];
			newPixel.owner = @"KImageLayer::initWithLayer:andMask";
			[self setPixel:newPixel atX:x y:y];
			[newPixel release];
		}
		[backgroundPixel release];
		
		name = [[NSString stringWithFormat:@"%@ (Copy)", layer.name] retain];
		opacity = 1.0;
		visible = YES;
		blendMode = NormalBlendMode;
		type = MaskLayer;
	}
	
	return self;
}

- (void) fillWithPixels: (KPixel*) samplePixel
{
	ForEachImagePixel(self)
	{
		samplePixel.owner = @"KImageLayer::fillWithPixels";
		[self setPixel:samplePixel atX:x y:y];
	}
}

- (KPixel*) pixelAtX: (NSUInteger)x y: (NSUInteger)y
{
	NSUInteger index = y * width + x;
	
	return pixels[index];
}

- (void) setPixel: (KPixel*)pixel atX: (NSUInteger)x y: (NSUInteger)y
{
	NSUInteger index = y * width + x;
	
	[pixels[index] autorelease];
	
	pixels[index] = [pixel retain];
}

- (void) dealloc
{
	for (int i = 0; i < width * height; i++)
	{	
		[pixels[i] release];
	}
	
	[name release];
	free(pixels);
	[super dealloc];
}

@end
