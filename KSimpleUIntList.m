//
//  KSimpleUIntList.m
//  Kia
//
//  Created by Andrey on 04/05/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import "KSimpleUIntList.h"

#define PREALLOCATED_ITEMS 10
#define ALLOCATION_STEP 5

@implementation KSimpleUIntList
@synthesize data;
@synthesize count;

- (id) init
{
	if (self = [super init])
	{
		preallocatedItemCount = PREALLOCATED_ITEMS;
		data = (NSUInteger*)malloc(sizeof(NSUInteger) * preallocatedItemCount);
		bzero(data, sizeof(NSUInteger) * preallocatedItemCount);
		count = 0;
	}
	
	return self;
}

+ (KSimpleUIntList*) list
{
	return [[[KSimpleUIntList alloc] init] retain];
}

- (id) initWithCapacity: (NSUInteger)capacity
{
	if (self = [super init])
	{
		preallocatedItemCount = capacity;
		data = (NSUInteger*)malloc(sizeof(NSUInteger) * preallocatedItemCount);
		bzero(data, sizeof(NSUInteger) * preallocatedItemCount);
		count = 0;
	}
	
	return self;
}

+ (KSimpleUIntList*) listWithCapacity: (NSUInteger)capacity
{
	return [[[KSimpleUIntList alloc] initWithCapacity:capacity] retain];
}

- (void) add: (NSUInteger)value
{
	if (count == preallocatedItemCount)
	{
		preallocatedItemCount += ALLOCATION_STEP;
		data = realloc(data, preallocatedItemCount);
	}
	
	data[count] = value;
	count++;
}

- (void) removeAllElements
{
	count = 0;
}

- (void) dealloc
{
	free(data);
	data = NULL;
	count = 0;
	preallocatedItemCount = 0;
	
	[super dealloc];
}

@end
