//
//  HistogramView.h
//  Kia
//
//  Created by Andrey on 15/03/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "KiaController.h"

@interface HistogramView : NSView 
{
	IBOutlet KiaController* controller;
	NSUInteger highlightedInterval; 
}

@property NSUInteger highlightedInterval;

- (id) initWithFrame: (NSRect)frame;
- (void) drawRect: (NSRect)rect;
- (BOOL) isFlipped;
- (void) dealloc;
@end
