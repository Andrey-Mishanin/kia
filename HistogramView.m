//
//  HistogramView.m
//  Kia
//
//  Created by Andrey on 15/03/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import "HistogramView.h"
#import "KImage.h"
#import "KCumulativeHistogram.h"
#import "KSegmentationMap.h"
#import "KSimpleFloatArray.h"
#import "KiaController.h"

@implementation HistogramView

@synthesize highlightedInterval;

- (id)initWithFrame: (NSRect)frame 
{
    self = [super initWithFrame:frame];

    if (self) 
	{
    }
    return self;
}

- (BOOL) isFlipped
{
	return FALSE;
}

- (BOOL) acceptsFirstResponder
{
	return YES;
}

- (void)keyDown:(NSEvent *)theEvent
{
	if ([theEvent keyCode] == 16 && [theEvent modifierFlags] | NSCommandKeyMask)
	{
		[[NSSavePanel savePanel] beginSheetForDirectory:@"/Users/Andrey/MSTU/Master/Paper/Design" 
												   file:nil 
										 modalForWindow:controller.histogramWindow 
										  modalDelegate:self 
										 didEndSelector:@selector(savePanelDidEnd:returnCode:contextInfo:) 
											contextInfo:NULL];
	}
	
	return;
}

- (void)savePanelDidEnd:(NSOpenPanel *)panel returnCode:(int)returnCode contextInfo:(void*)contextInfo
{
	if (returnCode == NSOKButton)
	{
		[controller.currentImage.histogram saveToTikzFile:[panel filename] forChannel:controller.displayedHistogramChannel];
	}
}

- (void)drawRect: (NSRect)rect 
{
	KHistogram* histogram;
	
	if (controller.displayCumulativeHistogram == NO)
		histogram = controller.currentImage.histogram;
	else
		histogram = controller.currentImage.cumulativeHistogram;
	
	enum KImageChannel channel = controller.displayedHistogramChannel;
	
	if (histogram != nil)
	{
	    NSBezierPath* path = [NSBezierPath bezierPath];
		[[NSColor lightGrayColor] set];
		
		CGFloat scalingFactor = 
		rect.size.height / histogram.channelHistograms[channel].maxPixelsPerInterval;
		
		NSInteger drawingPositionX = 2;
		[path moveToPoint:NSMakePoint(drawingPositionX, 5)];
		for (int i = 0; i < histogram.intervalCount; i += histogram.intervalCount / 256)
		{
			NSUInteger histogramIntervalValue = 
			histogram.channelHistograms[channel].data[i];
			
			if (controller.displayCumulativeHistogram == NO)
				[path moveToPoint:NSMakePoint(drawingPositionX, 5)];
			
			[path lineToPoint:
			 NSMakePoint(drawingPositionX, 5 + histogramIntervalValue * scalingFactor)];
			
			drawingPositionX++;
		}
		
		[path stroke];
		
		// Draw mean point
		[path removeAllPoints];
		NSInteger meanPoint = histogram.channelHistograms[channel].mean * 256;
		[path moveToPoint:NSMakePoint(meanPoint, 5)];
		[path lineToPoint:NSMakePoint(meanPoint, rect.size.height)];
		[[NSColor whiteColor] set];
		[path stroke];
		
		// Draw centroids
		if (controller.segmentationMap != nil)
		{
			[path removeAllPoints];
			for (NSUInteger i = 0; i < controller.segmentationMap.centroids.count; i++)
			{
				CGFloat centroidPoint = 
				controller.segmentationMap.centroids.data[i] * 256;
				
				[path moveToPoint:NSMakePoint(centroidPoint, 5)];
				[path lineToPoint:NSMakePoint(centroidPoint, rect.size.height)];
			}
			
			[[NSColor greenColor] set];
			[path stroke];
		}
		// Highlight interval
		CGFloat highlightX = 2 + highlightedInterval * histogram.intervalCount / 256;
		CGFloat highlightY = 4;
		[path removeAllPoints];
		path = 
		[NSBezierPath bezierPathWithRect:NSMakeRect(highlightX, highlightY, 1, 4)];
		[[NSColor whiteColor] set];
		[path fill];
		
		// Draw hue clue
		if (controller.displayedHistogramChannel == HueImageChannel)
		{
			NSArray* rainbowColors = [NSArray arrayWithObjects:
			 [NSColor redColor], [NSColor yellowColor], [NSColor greenColor],
			 [NSColor cyanColor], [NSColor blueColor], [NSColor magentaColor], 
			 [NSColor redColor], nil];
			
			NSGradient* rainbow = [[NSGradient alloc] initWithColors:rainbowColors];
			NSRect rainbowRect = NSMakeRect(2, 4, rect.size.width, 1);
			[rainbow drawInRect:rainbowRect angle:0.0];
			[rainbow release];
		}
	}
}

- (void) dealloc
{
	[super dealloc];
}
@end
