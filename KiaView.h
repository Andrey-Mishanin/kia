//
//  KiaView.h
//  Kia
//
//  Created by Andrey on 03/02/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "KiaController.h"

@interface KiaView : NSView 
{
	IBOutlet KiaController* controller;
}

- (id) initWithFrame: (NSRect)frame;
- (void) drawRect: (NSRect)rect;
- (void) dealloc;
@end