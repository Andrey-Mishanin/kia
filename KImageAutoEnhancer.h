//
//  KImageAutoEnhancer.h
//  Kia
//
//  Created by Andrey on 28/05/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class KImage;
@class K2DSegmentationMap;

@interface KImageAutoEnhancer : NSObject 
{
	KImage* imageToEnhance;
	K2DSegmentationMap* segmentationMap;
}

- (id) initWithImage:(KImage*)image andSegmentationMap:(K2DSegmentationMap*)map;
+ (KImageAutoEnhancer*) autoEnhancerWithImage:(KImage*)image andSegmentationMap:(K2DSegmentationMap*)map;
- (void) autoEnhanceWithImportantSegment: (NSUInteger)segment;

@end
