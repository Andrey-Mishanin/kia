//
//  KSimpleUIntArray.h
//  Kia
//
//  Created by Andrey on 01/04/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface KSimpleUIntArray : NSObject 
{
	NSUInteger* data;
	NSUInteger count;
}

@property (readonly) NSUInteger* data;
@property (readonly) NSUInteger count;

- (id) init;
- (id) initWithCapacity: (NSUInteger)capacity;
+ (KSimpleUIntArray*) arrayWithCapacity: (NSUInteger)capacity;
- (void) dealloc;

@end
