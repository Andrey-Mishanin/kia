//
//  KHistogram.h
//  Kia
//
//  Created by Andrey on 22/03/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

typedef struct _KChannelHistogram
{
	NSUInteger* data;
	NSUInteger maxPixelsPerInterval;
	CGFloat mean;
	CGFloat variance;
	CGFloat uniformity;
} KChannelHistogram, *KChannelHistogramRef, *KChannelHistogramArray;

@class KImage;
@class KSimpleUIntArray;

@interface KHistogram : NSObject 
{
	NSUInteger intervalCount;
	NSUInteger channelCount;
	KChannelHistogramArray channelHistograms;
	NSUInteger square;
	CGFloat colorContrast;
}

@property (readonly) NSUInteger intervalCount;
@property (readonly) CGFloat colorContrast;
@property (readonly) KChannelHistogramArray channelHistograms;

- (id) initWithIntervalCount: (NSUInteger)numIntervals andChannelCount: (NSUInteger)numChannels;
- (void) computeForImage: (KImage*)image;
- (void) computeForSegment: (NSUInteger)segment ofImage:(KImage*)image withCrispSegmentationMap: (KSimpleUIntArray*)crispSegmentationMap;
- (void) computeStatistics;
- (void) saveToTikzFile: (NSString*)tikzFilename forChannel: (enum KImageChannel)channel;
- (void) saveCircularHueHistogramToTikzFile: (NSString*)tikzFilename;
- (enum KImageChannel) maxVarianceChannel;
- (void) dealloc;

@end
