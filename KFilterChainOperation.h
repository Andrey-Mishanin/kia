//
//  KFilterChainOperation.h
//  Kia
//
//  Created by Andrey on 06/05/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class KImage;
@class KFilter;

@interface KFilterChainOperation: NSOperation 
{
	KImage* imageToProcess;
	NSArray* filterChain;
}

- (id) initWithImage: (KImage*)image andFilterChain: (NSArray*)aFilterChain;

@end
