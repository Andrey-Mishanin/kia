//
//  KSimpleFloatArray.h
//  Kia
//
//  Created by Andrey on 18/04/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface KSimpleFloatArray : NSObject 
{
	CGFloat* data;
	NSUInteger count;
}

@property (readonly) CGFloat* data;
@property (readonly) NSUInteger count;

- (id) init;
- (id) initWithCapacity: (NSUInteger)capacity;
+ (KSimpleFloatArray*) arrayWithCapacity: (NSUInteger)capacity;
- (CGFloat) maxValue;
- (NSUInteger) maxValueIndex;
- (CGFloat) minValue;
- (NSUInteger) minValueIndex;
- (void) dealloc;

@end