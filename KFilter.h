//
//  KFilter.h
//  Kia
//
//  Created by Andrey on 06/05/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#define GetParameterFloatValue(parameter) [[parameters objectForKey:parameter] floatValue]
#define GetParameterUIntValue(parameter) [[parameters objectForKey:parameter] unsignedIntegerValue]

enum KFilterType
{
	AutoLevels,
	Curves,
	Equalize,
	Desaturate
};

@class KPixel;
@class KImage;

@interface KFilter : NSObject 
{
	NSDictionary* parameters;
	KImage* currentImage;
}

- (id) initWithParameters: (NSDictionary*) filterParameters;
+ (KFilter*) filterWithParameters: (NSDictionary*) filterParameters;
- (KPixel*) processPixel: (KPixel*)pixel ofImage: (KImage*)image atX: (NSUInteger)x y: (NSUInteger)y;
- (void) reset;
- (KImage*) processImage: (KImage*)image inPlace: (Boolean)inPlace;
- (NSString*) name;
+ (NSString*) getNameByType: (enum KFilterType)type;
- (void) dealloc;

@end
