//
//  KCurve.h
//  Kia
//
//  Created by Andrey on 03/04/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#define MAX_CURVE_CONTROL_POINT_COUNT 16
typedef struct _CubicSpline 
{
	CGFloat a;
	CGFloat b;
	CGFloat c;
	CGFloat d;
	CGFloat t0;
} CubicSpline;

@interface KCurve : NSObject 
{
	NSMutableArray* controlPoints;
	CubicSpline* splines;
	Boolean needRecomputeCurve;
}

@property (readonly) NSArray* controlPoints;

- (id) init;
- (void) addControlPoint: (NSPoint) point;
- (void) replaceControlPointAt: (NSUInteger)index withPoint: (NSPoint)point;
- (void) removeControlPointAt: (NSUInteger)index;
- (void) removeControlPoint: (NSPoint) point;
- (void) removeAllControlPoints;
- (void) recomputeCurve;
- (CGFloat) valueAt: (CGFloat)t;
- (void) saveToTikzFile: (NSString*)filename;
- (void) dealloc;
@end
