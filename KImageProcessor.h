//
//  KImageProcessor.h
//  Kia
//
//  Created by Andrey on 28/05/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "KFilter.h"

@class KImage;

@interface KImageProcessor : NSObject 
{

}

+ (KFilter*) getDefaultFilterOfType: (enum KFilterType)type;
+ (void) processImage:(KImage*)image usingFilter:(KFilter*)filter;
+ (void) processImage:(KImage*)image usingFilterChain:(NSArray*)filterChain;
+ (Boolean) processImage:(KImage*)image usingTemporaryLayerForFilter:(KFilter*)filter withMultiplier: (CGFloat)multiplier;

@end
