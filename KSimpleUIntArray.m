//
//  KSimpleUIntArray.m
//  Kia
//
//  Created by Andrey on 01/04/2009.
//  Copyright 2009 Karma Software. All rights reserved.
//

#import "KSimpleUIntArray.h"


@implementation KSimpleUIntArray

@synthesize data;
@synthesize count;

- (id) init
{
	if (self = [super init])
	{
		data = NULL;
		count = 0;
	}
	
	return self;
}

- (id) initWithCapacity: (NSUInteger)capacity
{
	if (self = [super init])
	{
		count = capacity;
		data = (NSUInteger*)malloc(sizeof(NSUInteger) * capacity);
		bzero(data, sizeof(NSUInteger) * capacity);
	}
	
	return self;
}

+ (KSimpleUIntArray*) arrayWithCapacity: (NSUInteger)capacity
{
	KSimpleUIntArray* newArray = [[KSimpleUIntArray alloc] initWithCapacity:capacity];
	
	return [newArray autorelease];
}

- (void) dealloc
{
	free(data);
	data = NULL;
	count = 0;
	
	[super dealloc];
}

@end
